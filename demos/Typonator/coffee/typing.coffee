'use strict'

LETTERS_BEFORE_INCREASE = 20
INITIAL_TIME_BETWEEN_LETTERS = 1000
SPEED_INCREASE = 0.1
TIME_BETWEEN_SHIFTS = 100
PENALTY_FOR_MISS = 1
MAX_LETTERS = 100

started = false
setUp = false

shiftLettersInterval = -1
addLetterInterval = -1
timeBetweenLetters = INITIAL_TIME_BETWEEN_LETTERS

typingArea = null
scoreBoard = null
startButton = null

letterSpans = [MAX_LETTERS + 1]
totalLetterCount = 0
increaseCounter = 0

score = 0

#One time setup
initialize = ->
  $(document).on 'keyup', onKeyPress

  typingArea = $('#typingArea')
  scoreBoard = $('#scoreBoard')
  startButton = $('#startButton').on 'click', startGame

  #Create all of the divs
  #Note that they're created and prepended in reverse
  #order so that the 0 index is the left-most div
  #The div in MAX_LETTERS is special because is hidden so that
  #the last character doesn't appear outside of the box
  letterSpans[MAX_LETTERS] = $("<div class='letter' style='display: none;'> </div>")
  typingArea.prepend letterSpans[MAX_LETTERS]
  for index in [MAX_LETTERS - 1..0] by -1
    letterSpans[index] = $("<div class='letter'> </div>")
    typingArea.prepend letterSpans[index]

  setUpGame()

#Performs set up needed both before first game and
#before subsequent games
setUpGame = ->
  if started or setUp then return
  $("#gameControls").css 'visibility', 'hidden'
  $("#setupControls").css 'visibility', 'visible'
  for index in [0..MAX_LETTERS]
    letterSpans[index].text ' '
  changeScore 0
  increaseCounter = 0
  shiftCounter = 0
  timeBetweenLetters = INITIAL_TIME_BETWEEN_LETTERS
  setUp = true

#Returns an object containing an uppercase English character
#as well as the character's numeric ASCII code
getRandomLetter = ->
  #Get ASCII value in range for uppercase chars
  ascii = Math.floor(Math.random() * 25 + 65)
  return {} =
    code: ascii
    letter: String.fromCharCode ascii

#Handles adding a new letter. User can potentially lose
#during this function, so it checks for that case.
#Sticks the new letter into the DOM as needed
addLetter = ->
  result = getRandomLetter()
  if letterSpans[0].text() isnt '' then shiftLettersRight
  letterSpans[0].text result.letter
  #Skip the work below if the user has already lost
  if not checkForLoss()
    result.code -= 65
    tryIncreaseSpeed()

#Handles the logic for removing a letter, both from
#variables and from the DOM
removeLetter = (code)->
  #We now know the code is an uppercase letter
  #Convert it to 0-based index by subtracting 65
  letter = String.fromCharCode code
  code -= 65
  for index in [MAX_LETTERS - 1 .. 0] by -1
    if letterSpans[index].text() is letter
      letterSpans[index].text ' '
      changeScore score + 1
      return
  #If we didn't find a letter anywhere
  changeScore score - 1

#Shift all existing letters right by one
#Used when inserting a new letter and by
#a timed interval
shiftLettersRight = ->
  currentLetter = letterSpans[MAX_LETTERS].text()
  for index in [MAX_LETTERS..1] by -1
    leftLetter = letterSpans[index - 1].text()
    #Only write shift
    if leftLetter isnt ' '  or currentLetter isnt ' ' then letterSpans[index].text leftLetter
    currentLetter = leftLetter
  letterSpans[0].text ' '
  checkForLoss()
  return

#Handles all key input
#Assumes ASCII letters (a-zA-Z) plus ESC
onKeyPress = (e) ->
  #Only do things when the game is going
  if not started then return
  code = e.which
  #Special handling for ESC
  if code is 27
    endGame()
    return
  #Lowercase ASCII
  else if code >= 97 and code <= 122
    #Convert to uppercase
    code -= 32
  #Outside of letters, but not escape or lowercase = not a letter
  else if code < 65 or code > 90
    return

  removeLetter(code)

#Changes the score the value passed in
#Updates DOM as needed
changeScore = (newScore)->
  score = Math.max newScore, 0
  scoreBoard.text "Your Score: #{score}"

#Per spec, the speed should increase every so often
#This function handles the logic for that, including
#the DOM portions
tryIncreaseSpeed = ->
  increaseCounter += 1
  if increaseCounter < LETTERS_BEFORE_INCREASE then return false
  #Time to pump up the speed
  increaseCounter = 0
  timeBetweenLetters *= (1 - SPEED_INCREASE)
  #Set up the new interval
  clearInterval addLetterInterval
  addLetterInterval = window.setInterval addLetter, timeBetweenLetters
  return true

#Checks game conditions to see if user has lost.
#If so, activate endgame code
checkForLoss = ->
  if letterSpans[MAX_LETTERS].text() isnt ' '
    endGame()
    return true
  else
    return false

#Begins a new game by starting the interval
#Requires that setup has been completed previously
startGame = ->
  if started or not setUp then return
  $("#gameControls").css 'visibility', 'visible'
  $("#setupControls").css 'visibility', 'hidden'
  addLetterInterval = window.setInterval addLetter, timeBetweenLetters
  shiftLettersInterval = window.setInterval shiftLettersRight, TIME_BETWEEN_SHIFTS
  started = true

#Called either when the user presses ESC to end the game,
#or when the number of letters gets too high.
endGame = ->
  if not started then return
  started = false
  setUp = false
  clearInterval addLetterInterval
  clearInterval shiftLettersInterval
  window.alert "Game Over! Final score was #{score}."
  #Set it up for another game
  setUpGame()

$(document).ready initialize
