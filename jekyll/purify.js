var walk = require('walk');
var purify = require('purify-css');
var fs = require('fs');
var content = [];
var css = [];

// Walker options
var walker  = walk.walk('./public', { followLinks: false });

var contentMatcher = /.*(\.html|\.js)/;

walker.on('file', function(root, stat, next) {
    // Add this file to the list of files
    if (contentMatcher.test(stat.name)) {
      content.push(root + '/' + stat.name);
    }
    next();
});

function doIt(file, oldSize) {
  purify(content, [file], {
    minify: true,
    output: file,
  });
};

walker.on('end', function() {
  process.argv.forEach(function (val, index, array) {
    if (index > 1) {
      console.log("Purifying " + val);
      var oldSize = fs.statSync(val).size;
      doIt(val, oldSize);
    }
  });
});
