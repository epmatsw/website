#!/bin/bash
set -e

#Remove any leftovers
rm -rf public/*
find . -iname '*.min.css' -exec sh -c 'rm {}' \;
find . -iname '*.min.js' -exec sh -c 'rm {}' \;

echo "First round of minification"
#Set up includes with non-purified versions
find . -iname '*.css' -exec sh -c 'minify {} > /dev/null' \;
find . -iname '*.min.css' -exec sh -c 'mv {} _includes' \;
find . -iname '*.js' -exec sh -c 'minify {} > /dev/null' \;
find . -iname '*.min.js' -exec sh -c 'mv {} _includes' \;

echo "Second round of minification"
#Build with non-purified versions
jekyll build -q --drafts --config _config_dev.yml

#Remove old non-purified versions
find _includes -iname '*.min.css' -exec sh -c 'rm {}' \;

#Minify
find public -iname '*.css' -exec sh -c 'minify {} > /dev/null && rm {}' \;
find public -iname '*.js' -exec sh -c 'minify {} > /dev/null && rm {}' \;

#Purify
find public -iname '*.css' -exec sh -c 'node purify {} > /dev/null' \;

#Move purified CSS to _includes for rebuild
find public -iname '*.css' -exec sh -c 'mv {} _includes' \;

echo "Third round of minification"
#Build with purified versions
jekyll build -q --drafts --config _config_dev.yml

#Re-minify just in case
find public -iname '*.css' -exec sh -c 'minify {} > /dev/null && rm {}' \;
find public -iname '*.js' -exec sh -c 'minify {} > /dev/null && rm {}' \;

#Re-purify just in case
find public -iname '*.css' -exec sh -c 'node purify {} > /dev/null' \;

#Cleanup
find _includes -iname '*.css' -exec sh -c 'rm {}' \;
find _includes -iname '*.js' -exec sh -c 'rm {}' \;

cp -r ../demos public/demos

open http://blog.dev
