#!/bin/bash
set -e

# optipng
for png in `find assets -iname "*.png"`; do
    echo "crushing $png ..."
    optipng -o7 $png
done

# jpegtran
for jpg in `find assets -iname "*.jpg"`; do
    echo "crushing $jpg ..."
    jpegtran -copy none -optimize -perfect "$jpg" > temp.jpg

    # preserve original on error
    if [ $? = 0 ]; then
        mv -f temp.jpg $jpg
    else
        rm temp.jpg
    fi
done

