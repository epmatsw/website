---
layout: post
title: First Impressions of Android
categories: [android, review]
tags: [android, nexus7]
---
So I finally caved and used my tax return to buy myself a Nexus 7. I
figured I'd write down my impressions so that I can keep track of them.

Positive Impressions
--------------------
The hardware itself is a pleasure to use (for the most part). I really
like the rubber/plastic backing, and the whole device itself feels very
sturdy. The screen is also excellent as far as I can tell. I've tried
reading some books on it, and the pixel density is more than good enough
for it.

The feature that has most impressed me though is the text-to-speech
functionality. I find myself using the keyboard less frequently than
just saying whatever I want to type out loud, and that's not something
that I would ever do on iOS. The speed difference between Google's
text-to-speech and Siri is just unbelievable.

I'd always heard that Android's notification center was superior to the
iOS version, but I never really believed it. I have to say though, my
experience with the Android version has pretty much convinced me. Some
of the features that I added via jailbreak tweaks in iOS (settings in
the Notification Center in particular) come standard in Android, and
that's nice.

Finally, widgets are actually pretty neat. I've got a time/weather one
that takes up about half the screen on my homescreen, and it works well
for what I use my tablet for. I think if needed more homescreen space
for apps I would find it less useful, but for now, it works out fine.
I'm mildly concerned about what effects it has on battery life, but I'll
just have to wait an see on that.

Negative Impressions
--------------------
The first thing I noticed was the surprisingly poor performance in
general usage. I use an iPhone 5 daily, and it was surprising to see a
larger device stutter while scrolling even simple webpages. I suppose
part of the issue with this was that I hadn't done research and
reaslized that the Nexus 7 hardware is less powerful than the iPhone 5,
but still, basic UI operations are significantly less smooth. I'd always
heard Android had issues with smoothness, but I assumed they were
exaggerated. I have to say, my experience so far has pretty much
confirmed those issues in my mind. I can't even imagine using older
versions of Android if this is an improved smoothness experience.

I was less impressed with Google Now than the reviews it's gotten would
have lead me to believe. I keep seeing it referred to as Android's
"killer feature", but it hasn't really added any value for me. Right now, it's basically a weather widget. I mainly use the tablet in my apartment, so
maybe it would be more useful on the road.

Another thing that sort of worried me was that one of the top app
recommendations I received was for an anti-virus application. So far it
hasn't encountered anything, but the fact that it's
necessary/recommended worries me.

One problem that I encountered that frustrated me was the inability to
delete empty screens from the stock launcher. It seemed like such a
basic feature, but all the advice I saw online basically said to just
replace the launcher. It's a bit of a hassle to have to find a third
party solution to get basic functionality, but I guess that's the point
of Android being customizable.

Finally, on a hardware note, the buttons on the side of the device are
really inconveniently placed, and their motion is not very good. I'd
prefer it if they had a bit more click to them or if they moved a bit
more.

Reserving Judgement
-------------------
Right now, I still miss having a dedicated home button on the front of
the device. Reaching around the side to turn on the device still seems a
bit less efficient and inconvenient to me.

I'm also not entirely sold yet on the general design aesthetic of
Android apps. I'm not a huge fan of the flat or rectangular school of
design, and the apps that I've found most easy to use are the ones that
mimic their iPhone counterparts rather than sticking with the default
Android styles. The edges of buttons seem poorly defined, and I feel
like I'm navigating more by guesswork than by using cues from the
interface. I have trouble imagining, for example, my grandma figuring
out how to use this interface.

Overall Impression
------------------
I think Android turned out better than I expected. There are some really
neat bits, but there are also some warts. I guess if I had to summarize
it, it would be that my iPhone does 95% of what I want to do at an A to
A- level, while Android does 95% of what I want to do at a B to B-
level. The difference would be that I can improve those grades and get
up to 100% by replacing and adding on, while iOS is pretty much limited
to what you've got. I'm still not sure which one I prefer yet, but I
guess I'll see.
