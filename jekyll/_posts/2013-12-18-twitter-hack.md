---
layout: post
title: I've Been Hacked!
categories: [general]
tags: [twitter]
---
Last Friday afternoon, I jumped on [my Twitter](https://twitter.com/epmatsw) for the first time in a
few months to follow a speaker whose talk I attended that morning.
When I went to see if he had tweeted anything the next Monday morning, I discovered that someone had gained access
to my account and executed an interesting and novel (at least to me) hack.

The Results
-----------
To my great surprise, my entire twitter feed seemed to be in a foreign language!

![My very confusing Twitter feed](){: class="lazyload" data-src="{{ site.dirs.imgs }}hijacked.png" width="526" height="919"}

That was unusual, but perhaps some of the people I follow were just retweeting
things? Nope, I genuinely appeared to be following ~50 minor/aspiring
celebrities from Asia and the Middle East. I immediately changed my
password, unfollowed the last 50 people added, and then proceeded to see what else had been done to my
precious, rarely-used Twitter. The answer: nothing. Literally nothing.
No tweets posted, no DMs sent, no devices authorized, recovery email
untouched.
I've been hacked once before, and the result was that my account tweeted
some bogus links and DMed some of my followers to try to phish some
people. Obviously, that lead those people to text me and alert me of the
situation, allowing me to fix the situation immediately. If I hadn't
noticed the immediate change of language in my feed, this hack could
have continued pretty much indefinitely.

How Did This Happen?
--------------------
I reused a password. Yes, the cardinal sin of password management. It
was a fairly complex password that I used at a few sites but had slowly
been phasing out. I checked to see if that password had been [part of any
recent leaks](https://lastpass.com/adobe/), but it doesn't seem to be
anywhere that I've found. And as far as I know, the email account and
the Twitter client I was using are both still secure. It must have
leaked somewhere though.

What Was the Point?
-------------------
As I said, the people that were added appeared to be mostly unknown singers,
performers, or bloggers. I suspect that the hack was an extremely
aggressive form of advertising, basically forcing me to be exposed to
those people. It would probably be interesting to email some of the
people I was forced to follow and
see if they're aware that whoever they're paying is actually doing this,
but sadly I don't speak Korean or Arabic.

Ways It Could Have Been Better (Or Worse)
-----------------------------------------
Really, the best way to execute the attack would be to 1) actually make
sure the person who you're attacking can read the tweets you're forcing
on them and 2) add people more slowly. I would guess that they didn't
bother with these approaches since they just wanted to do it quick and
dirty before they lost the opportunity. Anyways, it was an interesting
experience, and it finally forced me to get rid of my last use of that old password!
