---
layout: post
title: Automatic - First Impressions
categories: [gadgets, review]
tags: [automatic]
---
I got my [Automatic](http://www.automatic.com/) last week. So far, it's
been okay. There are some good things and some bad, but I figured I'd
get my thought put down first.

The Good Things
---------------
First, the iOS app is pretty excellent. It fits well with iOS 7, and it was
clearly built from the ground up rather than using pre-built components.
The small animations in the application are truly excellent and it's a
general pleasure to use.

The hardware itself seems solid. No moving parts, just plug and go.

The setup process was pretty easy. The on-screen instructions were
clear. I felt like there could have been fewer steps necessary to
accomplish the same thing, but I understand the security concerns that
mandate a few more steps than I would have appreciated.

The Bad Things
--------------
Unfortunately, the bad list is going to be longer than the good list,
just by the virtue of this being a fairly simple product early in its
lifecycle.

My biggest complaint is that the warnings don't seem to be consistent in
any meaninful way. The acceleration alarm only triggers from a dead
stop, and it triggers on relatively common accelerations. Pull a left
turn at a stoplight? Alarm. Go right at a stop sign? Alarm. Accelerate
from 5 MPH to 70 MPH by stomping on the gas while turning onto a
freeway? No alarm. The breaking alarm seems to happen pretty much every
yellow light I encounter. I still haven't figured out exactly how the
speeding alarm works. Does it disable itself after 3 beeps? Does it
still keep counting and just not make any more noise? Regardless of all
of that, none of it seems to actually record in the app itself. I get 3
to 5 beeps on the way to work every day and am rocking a 97 score. I
don't know who's driving bad enough to actually get a failing score, but
may God have mercy on their souls. Better
explanations and more customization would make this a bit more enjoyable.

The hardware is bright white. Like, Apple in 2004 plastic white. I don't
know about you, but I've never seen a car interior that is plain iPod
white. In my car (a 2009 Mazda 6), the port it plugs into is configured
so that it has to stick out a bit, but they certainly didn't try to make
the hardware blend in. It's got a nice white-with-metallic-accents mid
2000s vibe going on and just seems a bit out of time. I notice it every
time I get into my car.

If you are like me and live up north, this next point might matter to
you. Both my home parking-space and work parking-space are inside of
underground concrete garages. As a result, my phone reception is
essentially nothing when I park my car. At this point, if I open up the
Automatic app, it spends 30 seconds to a minute trying to determine my
location and save it before displaying the stats for my drive. I get why
it would make sense to save the location ASAP, but it would be nice if I
could just immediately pull up a scoredboard for my latest drive rather
than sitting around for it to realize that the location thing is never
going to happen.

The sounds that the device makes are fairly harsh. I listen to music
fairly loudly while driving to work, and I still notice the noises it
makes. That seems intentional, but it would be nice if the speeding
sound wasn't a piercing squeak. I'm considering disabling the sounds
just because they're so annoying.

Finally, a couple of small complaints about the iOS app. There's only
two buttons on the home screen of the application. One takes you to the
maintenance screen, and one takes you to the settings screen. Neither
seems like an option you would choose often. It sort of just seems like
a waste of space in general. Cluttering up the UI. I also have not yet
found a way to download the raw data for my drives. FitBit requires a
Premium membership to get a CSV file of your raw data, and it wouldn't
surprise me if Automatic is planning something similar. It seems like a
feature that is a logical next step but just hasn't been implemented
yet.

Conclusions
-----------
Overall, it's a decent product. I really do like seeing how much money
I'm spending on gas, and the iOS app shows that they are very dedicated
to building a beautiful experience. I do feel myself adjusting my
driving style to avoid the annoying beeping noises it can sometimes
generate. There's just some early-adopter
issues that haven't been addressed or polished away yet. Once/if they
ever get those issues figured out, Automatic will be well worth the
investment.
